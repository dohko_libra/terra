/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sena.terra.interfaces;

import com.sena.terra.dto.ClasificacionDto;
import java.util.List;

/**
 *
 * @author Usuario
 */
public interface IClasificacionDao {
    
    public void guardar(final ClasificacionDto clasificaccionDto);
    
    public void delete(final ClasificacionDto clasificacionDto);
    
    public List<ClasificacionDto> listaClasificacion();
    
    
    
}
