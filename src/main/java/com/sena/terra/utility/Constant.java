/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sena.terra.utility;

/**
 *
 * @author Usuario
 */
public class Constant {

    public static final int ZERO = 0;
    public static final int ONE = 1;
    public static final int TWO = 2;
    public static final int THREE = 3;
    public static final int FOUR = 4;
    public static final int FIVE = 5;

    /**
     * Estado de solicitud
     */
    public static final String NUEVA = "NUEVA";

    /**
     * paramAbrev
     */
    public static final String PARAM_TPC = "TPC";
    public static final String PARAM_TPD = "TPD";
    public static final String PARAM_PQR = "PQR";
    public static final String PARAM_CDD = "CDD";
    
    
    /**
     * template mail
     */
    
    public static final String LOGO_TERRA = "#LOGO_TERRA";
    public static final String TITULO_CONTENIDO = "#TITULO_CONTENIDO";
    
    public static final String TEXTO_NOTIFICACION_SOLICITUD = "#TEXTO_NOTIFICACION_SOLICITUD";
    public static final String TEXTO_CAMBIO_INICIO_SESION = "#TEXTO_CAMBIO_INICIO_SESION";
    public static final String PASSWORD = "#PASSWORD";
    
    /**
     * template generico
     */
    public static final String CONTENIDO_MAIL = "#TEXTO_CONTENIDO_MAIL";
    
}
