/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sena.terra.conversor;

import com.sena.terra.dto.ClasificacionDto;
import com.sena.terra.dto.SolicitudDto;
import com.sena.terra.entity.Solicitud;
import com.sena.terra.utility.Validator;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Usuario
 */
public class ClasificacionConversor {

    public static List<ClasificacionDto> listaSolicitudFromEntity(final List<Solicitud> listaClasificacion) {
        final List<ClasificacionDto> listaClasificacionDto = new ArrayList<>();
        if (Validator.isListNull(listaClasificacion)) {
            return new ArrayList<>();
        } else {
            listaClasificacion.stream().forEach(clasificacion -> {
                listaClasificacionDto.add(solicitudFromEntity(clasificacion));
            });
        }
        return listaClasificacionDto;
    }

    public static ClasificacionDto solicitudFromEntity(final Solicitud clasificacion) {
        if (null == clasificacion) {
            return null;
        } else {
            final ClasificacionDto clasificacionDto = new ClasificacionDto();
            clasificacionDto.setCantidadVacantes(clasificacion.getCantidadVacantes());
            clasificacionDto.setDescripcion(clasificacion.getDescripcion());
            clasificacionDto.setEstado(clasificacion.getEstado());
            clasificacionDto.setIdCLASIFICACION(clasificacion.getIdSOLICITUD());
            clasificacionDto.setLugarTrabajo(clasificacion.getLugarTrabajo());
            clasificacionDto.setNumeroRadicado(clasificacion.getNumeroRadicado());
            clasificacionDto.setCargoDto(CargoConversor
                    .cargoFromEntity(clasificacion.getIdCARGO()));
            return clasificacionDto;
        }
    }

    public static Solicitud solicitudFromDto(final SolicitudDto solicitudDto) {
        if (null == solicitudDto) {
            return null;
        } else {
            final Solicitud solicitud = new Solicitud();
            solicitud.setCantidadVacantes(solicitudDto.getCantidadVacantes());
            solicitud.setDescripcion(solicitudDto.getDescripcion());
            solicitud.setEstado(solicitudDto.getEstado());
            solicitud.setIdSOLICITUD(solicitudDto.getIdSOLICITUD());
            solicitud.setLugarTrabajo(solicitudDto.getLugarTrabajo());
            solicitud.setNumeroRadicado(solicitudDto.getNumeroRadicado());
            solicitud.setIdCARGO(CargoConversor.cargoFromEntity(solicitudDto.getCargoDto()));
            return solicitud;
        }
    }

    public static Solicitud clasificacionFromDto(ClasificacionDto clasificacionDto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
