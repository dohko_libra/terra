/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sena.terra.conversor;

import com.sena.terra.dto.ContratacionDto;
import com.sena.terra.dto.SolicitudDto;
import com.sena.terra.entity.Solicitud;
import com.sena.terra.utility.Validator;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Usuario
 */
public class ContratacionConversor1 {

    public static List<ContratacionDto> listaSolicitudFromEntity(final List<Solicitud> listaContratacion) {
        final List<ContratacionDto> listaContratacionDto = new ArrayList<>();
        if (Validator.isListNull(listaContratacion)) {
            return new ArrayList<>();
        } else {
            listaContratacion.stream().forEach(contratacion -> {
                listaContratacionDto.add(solicitudFromEntity(contratacion));
            });
        }
        return listaContratacionDto;
    }

    public static ContratacionDto solicitudFromEntity(final Solicitud contratacion) {
        if (null == contratacion) {
            return null;
        } else {
            final ContratacionDto contratacionDto = new ContratacionDto();
            contratacionDto.setCantidadVacantes(contratacion.getCantidadVacantes());
            contratacionDto.setDescripcion(contratacion.getDescripcion());
            contratacionDto.setEstado(contratacion.getEstado());
            contratacionDto.setIdCONTRATACION(contratacion.getIdSOLICITUD());
            contratacionDto.setLugarTrabajo(contratacion.getLugarTrabajo());
            contratacionDto.setNumeroRadicado(contratacion.getNumeroRadicado());
            contratacionDto.setCargoDto(CargoConversor
                    .cargoFromEntity(contratacion.getIdCARGO()));
            return contratacionDto;
        }
    }

    public static Solicitud solicitudFromDto(final SolicitudDto solicitudDto) {
        if (null == solicitudDto) {
            return null;
        } else {
            final Solicitud solicitud = new Solicitud();
            solicitud.setCantidadVacantes(solicitudDto.getCantidadVacantes());
            solicitud.setDescripcion(solicitudDto.getDescripcion());
            solicitud.setEstado(solicitudDto.getEstado());
            solicitud.setIdSOLICITUD(solicitudDto.getIdSOLICITUD());
            solicitud.setLugarTrabajo(solicitudDto.getLugarTrabajo());
            solicitud.setNumeroRadicado(solicitudDto.getNumeroRadicado());
            solicitud.setIdCARGO(CargoConversor.cargoFromEntity(solicitudDto.getCargoDto()));
            return solicitud;
        }
    }

    public static Solicitud contratacionFromDto(ContratacionDto contratacionDto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
