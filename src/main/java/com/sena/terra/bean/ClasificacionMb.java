package com.sena.terra.bean;

import com.sena.terra.dto.CargoDto;
import com.sena.terra.dto.ClasificacionDto;
import com.sena.terra.interfaces.ICargoDao;
import java.io.Serializable;
import java.util.List;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import com.sena.terra.interfaces.IClasificacionDao;
import com.sena.terra.utility.Constant;
import com.sena.terra.utility.MessageUtil;

/**
 *
 * @author Usuario
 */
@Named(value = "clasificacionMb")
@ViewScoped
public class ClasificacionMb implements Serializable {

    @Inject
    private IClasificacionDao clasificacion;

    private ClasificacionDto clasificacionDto;
    private List<CargoDto> listaCargos;
    private List<ClasificacionDto> listaSolicitudes;
    private List<ClasificacionDto> filter;

    private boolean isForm;

    /**
     * Creates a new instance of ClasificacionMb
     */
    public ClasificacionMb() {
        super();
    }

    public void guardar() {
        this.clasificacionDto.setEstado(Constant.NUEVA);
        clasificacion.guardar(this.clasificacionDto);
        listaSolicitudes = clasificacion.listaClasificacion();
        filter = listaSolicitudes;
        isForm = Boolean.FALSE;
        MessageUtil.addMessageInfo("Exitoso", "Se ha guardado correctamente");
    }

    public void clean() {
        if (this.getClasificacionDto().getIdCLASIFICACION() == Constant.ZERO) {
            this.setClasificacionDto(new ClasificacionDto());
            this.getClasificacionDto().setCargoDto(new CargoDto());
        } else {
            this.getClasificacionDto().setCantidadVacantes(Constant.ZERO);
            this.getClasificacionDto().setCargoDto(new CargoDto());
            this.getClasificacionDto().setDescripcion(null);
            this.getClasificacionDto().setLugarTrabajo(null);
        }
        this.isForm = Boolean.FALSE;
    }

    public void cleanNew() {
        this.setClasificacionDto(new ClasificacionDto());
        this.getClasificacionDto().setCargoDto(new CargoDto());
        this.isForm = Boolean.TRUE;
    }

    public void preEditar(final ClasificacionDto clasificacionDto) {
        this.clasificacionDto = clasificacionDto;
        this.isForm = Boolean.TRUE;
    }

    public void delete(final ClasificacionDto clasificacionDto) {
        this.clasificacion.delete(clasificacionDto);
        this.listaSolicitudes = clasificacion.listaClasificacion();
        filter = listaSolicitudes;
        MessageUtil.addMessageInfo("Exitoso", "Se ha eliminado correctamente");
    }

    /**
     * @return the solicitudDto
     */
    public ClasificacionDto getClasificacionDto() {
        return clasificacionDto;
    }

    /**
     * @param clasificacionDto the clasificacionDto to set
     */
    public void setClasificacionDto(ClasificacionDto clasificacionDto) {
        this.clasificacionDto = clasificacionDto;
    }

    /**
     * @return the listaCargos
     */
    public List<CargoDto> getListaCargos() {
        return listaCargos;
    }

    /**
     * @param listaCargos the listaCargos to set
     */
    public void setListaCargos(List<CargoDto> listaCargos) {
        this.listaCargos = listaCargos;
    }

    /**
     * @return the listaClasificacion
     */
    public List<ClasificacionDto> getListaClasificacion() {
        return listaSolicitudes;
    }

    /**
     * @param listaClasificacion the listaClasificacion to set
     */
    public void setListaClasificacion(List<ClasificacionDto> listaClasificacion) {
        this.listaSolicitudes = listaClasificacion;
    }

    /**
     * @return the isForm
     */
    public boolean isIsForm() {
        return isForm;
    }

    /**
     * @param isForm the isForm to set
     */
    public void setIsForm(boolean isForm) {
        this.isForm = isForm;
    }

    /**
     * @return the filter
     */
    public List<ClasificacionDto> getFilter() {
        return filter;
    }

    /**
     * @param filter the filter to set
     */
    public void setFilter(List<ClasificacionDto> filter) {
        this.filter = filter;
    }

}